1. add_compound.ipynb
	- poly_smiles()
		Create smiles string of a polymer
		Save the polymer to a .mol2 file

	- find_bond_site()
		iterates through smiles string sites looking for bonding atoms
		requires user input
		classify y/n bonding site
		classify type (poly or branch or both)
		classify how many bonds can form

	- add_component()
		creates .json file for structure (backbone or branch)
		if backbone, uses find_bond_sites to create bonding dict

2. polymer_from_smiles
	- poly_smiles()
		Creates smiles string of a polymer
		Uses smiles_count() (diff than poly_smiles in add_compound)
	- find_polysite()
		Less developed version of find_bond_site() in add_compound?
	- smiles_count()
		More robust method to count # of atoms in a smiles string
		Attemps to deal with two-digit atom names
		Maybe a better way to do this? Just use existing method to
		generate a molecule, count # of atoms, and list them?

3. polydispersity
	- No functions
		Trying to generate a list of polymer lengths that fit a given
		distribution

4. ptb7_mer
	- Looks like the o.g. polymer-creation script
		Can probably get rid of this one


